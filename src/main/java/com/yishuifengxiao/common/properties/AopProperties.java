package com.yishuifengxiao.common.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Aop相关的配置参数
 * 
 * @author yishui
 * @date 2020年6月17日
 * @version 1.0.0
 */
@ConfigurationProperties(prefix = "yishuifengxiao.aop")
public class AopProperties {

	/**
	 * 是否开启全局参数校验拦截,默认为true
	 */
	private Boolean enable = true;
    
	/**
	 * 获取是否开启全局参数校验拦截
	 * @return
	 */
	public Boolean getEnable() {
		return enable;
	}
    
	/**
	 * 设置是否开启全局参数校验拦截
	 * 
	 * @param enable
	 */
	public void setEnable(Boolean enable) {
		this.enable = enable;
	}
	
	
}
